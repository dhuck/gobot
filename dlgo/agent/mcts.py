from functools import partial
from multiprocessing import Pool
import numpy as np
import random

from dlgo.agent import Agent, FastRandomBot
from dlgo.gotypes import Player
from dlgo.utils import coords_from_point

__all__ = [
    'MCTSAgent',
]


def fmt(x):
    if x is Player.black:
        return 'B'
    if x is Player.white:
        return 'W'
    if x.is_pass:
        return 'pass'
    if x.is_resign:
        return 'resign'
    return coords_from_point(x.point)


def show_tree(node, indent='', max_depth=3):
    if max_depth < 0:
        return
    if node is None:
        return
    if node.parent is None:
        print('%sroot' % indent)
    else:
        player = node.parent.game_state.next_player
        move = node.move
        print('%s%s %s %d %.3f' % (
            indent, fmt(player), fmt(move),
            node.num_rollouts,
            node.winning_frac(player),
        ))
    for child in sorted(node.children, key=lambda n: n.num_rollouts, reverse=True):
        show_tree(child, indent + '  ', max_depth - 1)


class MCTSNode(object):
    def __init__(self, game_state, parent=None, move=None):
        self.game_state = game_state
        self.parent = parent
        self.move = move
        self.win_counts = {
            Player.black: 0,
            Player.white: 0,
        }
        self.num_rollouts = 0
        self.children = []
        self.unvisited_moves = game_state.legal_moves()

    def add_random_child(self):
        index = random.randint(0, len(self.unvisited_moves) - 1)
        new_move = self.unvisited_moves.pop(index)
        new_game_state = self.game_state.apply_move(new_move)
        new_node = MCTSNode(new_game_state, self, new_move)
        self.children.append(new_node)
        return new_node

    def record_win(self, winner):
        self.win_counts[winner] += 1
        self.num_rollouts += 1

    def can_add_child(self):
        return len(self.unvisited_moves) > 0

    def is_terminal(self):
        return self.game_state.is_over()

    def winning_frac(self, player):
        return float(self.win_counts[player]) / float(self.num_rollouts)

    def __str__(self):
        return f"""
    win counts: {self.win_counts}
    n_children: {len(self.children)}
    unvisited: {len(self.unvisited_moves)}
    can_add_child: {self.can_add_child()}
    is_terminal: {self.is_terminal()}
        """


class MCTSAgent(Agent):
    def __init__(self, num_rounds, temperature, warmup = 90):
        Agent.__init__(self)
        self.num_rounds = num_rounds
        self.temperature = temperature

    def selection_worker(self, node):
        while (not node.can_add_child()) and (not node.is_terminal()):
            node = self.select_child(node)

        # Add a new child node into the tree.
        if node.can_add_child():
            node = node.add_random_child()

        # Simulate a random game from this node.
        winner = self.simulate_random_game(node.game_state)
        
        # Propagate scores back up the tree.
        while node is not None:
            node.record_win(winner)
            node = node.parent

    def select_move(self, game_state):
        root = MCTSNode(game_state)
        
        for i in range(self.num_rounds):
            node = root
            self.selection_worker(node)
            
        scored_moves = [
            (child.winning_frac(game_state.next_player), child.move, child.num_rollouts)
            for child in root.children
        ]
        scored_moves.sort(key=lambda x: (x[0], x[2]), reverse=True)
        for s, m, n in scored_moves[:10]:
            print('%s - %.3f (%d)' % (m, s, n))

        # Having performed as many MCTS rounds as we have time for, we
        # now pick a move.
        best_move = scored_moves[0][1]
        if best_move.is_pass and scored_moves[0][2] < 20:
            best_move = scored_moves[1][1]
        return best_move

    def score_child(self, node, log_rollouts):
        win_pct = node.winning_frac(node.game_state.next_player)
        exp_fact = np.sqrt(log_rollouts / node.num_rollouts)
        uct_score = win_pct + self.temperature * exp_fact

        return (node, uct_score)

    def select_child(self, node):
        """Select a child according to the upper confidence bound for
        trees (UCT) metric.
        """
        total_rollouts = sum(child.num_rollouts for child in node.children)
        log_rollouts = np.log(total_rollouts)

        # with Pool() as pool:
        #     children = pool.map(partial(self.score_child, log_rollouts=log_rollouts), node.children)
        
        children = [self.score_child(child, log_rollouts) for child in node.children]
        children.sort(key=lambda x: x[1], reverse = True)
        return children[0][0]

    @staticmethod
    def simulate_random_game(game):
        # print("simulatin...")
        bots = {
            Player.black: FastRandomBot(),
            Player.white: FastRandomBot(),
        }
        while not game.is_over():
            bot_move = bots[game.next_player].select_move(game)
            game = game.apply_move(bot_move)
        winner = game.winner() 
        return winner
        