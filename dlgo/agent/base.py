__all__ = [
    'Agent',
    'AsyncAgent'
]

class Agent:
    def __init__(self) -> None:
        pass 

    def select_move(self, game_state):
        raise NotImplementedError()

class AsyncAgent:
    def __init__(self) -> None:
        pass 

    async def select_move(self, game_state):
        raise NotImplementedError()