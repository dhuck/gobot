from dlgo.agent.fast_naive_random import FastRandomBot
from dlgo.agent.mcts import MCTSAgent
from dlgo.agent.minimax import AlphaBetaAgent 
from dlgo.agent.naive_random import RandomBot
from dlgo.goboard import GameState
from dlgo.gotypes import Player, Point

from dlgo.utils import print_board, print_move
from dlgo.scoring import compute_game_result
import time

def capture_diff(game_state):
    black_stones = 0
    white_stones = 0
    for r in range(1, game_state.board.num_rows + 1):
        for c in range(1, game_state.board.num_cols + 1):
            p = Point(r, c)
            color = game_state.board.get(p)
            if color == Player.black:
                black_stones += 1
            elif color == Player.white:
                white_stones += 1
    diff = black_stones - white_stones
    if game_state.next_player == Player.black:
        return diff
    return -1 * diff

COLS = "ABCDEFGHIJKLMNOPQRST"

def main():
    board_size = 13
    wins = {
            Player.black: 0,
            Player.white: 0
            }
    for _ in range(1000):
        bots = {
            Player.black: MCTSAgent(board_size**2 * 16, 1.9, warmup=1),
            Player.white: MCTSAgent(board_size**2 * 16, 1.4, warmup=1),
        }
        game = GameState.new_game(board_size)
        print(chr(27) + "[2J") 
        print_board(game.board)
        print()
        while not game.is_over():
            time.sleep(0.01)
            bot_move = bots[game.next_player].select_move(game)
            # print(chr(27) + "[2J") 
            game = game.apply_move(bot_move)
            print_board(game.board)
            player = "White" if game.next_player is Player.black else "Black"
            if bot_move.is_pass:
                move = "pass"
            elif bot_move.is_resign:
                move = "resign"
            else:
                move = f"{COLS[bot_move.point.col - 1]}{bot_move.point.row:>2d}"
            print(f"{player}: {move:<15s}{wins[Player.black]:03d}-{wins[Player.white]:03d}")
        wins[game.winner()] += 1
        result = compute_game_result(game, komi=0.5)
        print(result)
        time.sleep(2)


if __name__ == '__main__':
    main()
